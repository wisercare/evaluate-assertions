const fetch = require('node-fetch')
const {evaluateAssertions} = require('./src/evaluate-assertions')

exports.handler = async (event, context) => {
    await evaluateAssertions(fetch, process.env.URL, process.env.METHOD)
};
