exports.evaluateAssertions = async function (fetch, url, httpMethod) {
  try {
    const response = await fetch(url, {method: httpMethod})
    const json = await response.json()
    console.log(JSON.stringify(json, null, 2))
  } catch (error) {
    console.log(error)
  }
}