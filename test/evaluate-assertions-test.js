'use strict';

const test = require('tape')
const {evaluateAssertions} = require('../src/evaluate-assertions');

test('evaluateAssertions', async t => {
    const response = {
        json: () => {
            t.pass('executes json on the response')
            return {test: 'test'}
        }
    }
    const fetch = (url, init) => {
        t.pass('executes fetch')
        t.deepEqual(url, 'url', 'executes fetch with the correct url')
        t.deepEqual(init, {method: 'method'}, 'executes fetch with the correct init')
        return Promise.resolve(response)
    }
    const url = 'url'
    const method = 'method'
    
    t.plan(4)

    await evaluateAssertions(fetch, url, method)

    t.end()
});
